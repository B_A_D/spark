package com.test.spark;

import com.test.spark.algorithm.LikeMindedPersonsAlgo;
import com.test.spark.algorithm.SameTypeOfPlacesAlgo;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class Main {
    public static void main(String[] args) {

        Options options = new Options();
        Option vfileOption = Option.builder()
                .longOpt("visits")
                .hasArg()
                .desc("Visits file path")
                .required(true)
                .build();
        options.addOption(vfileOption);

        Option sfileOption = Option.builder()
                .longOpt("sites")
                .hasArg()
                .desc("Sites file path")
                .required(true)
                .build();
        options.addOption(sfileOption);

        Option algoOption = Option.builder()
                .longOpt("algo")
                .hasArg()
                .desc("Type of algo will be used. Can be: 'same', 'like'")
                .required(true)
                .build();
        options.addOption(algoOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("spark", options);

            System.exit(1);
        }

        String visitsPath = cmd.getOptionValue("visits");
        String sitesPath = cmd.getOptionValue("sites");
        String algo = cmd.getOptionValue("algo");
        if (!algo.equals("same") && !algo.equals("like"))
            formatter.printHelp("spark", options);

        SameTypeOfPlacesAlgo sameTypeOfPlacesAlgo = new SameTypeOfPlacesAlgo();
//        String visitsPath = Main.class.getClassLoader().getResource("visits.txt").getFile();
        Dataset<Row> visits = sameTypeOfPlacesAlgo.loadVisits(visitsPath);
//
//        String sitesPath = Main.class.getClassLoader().getResource("sites.txt").getFile();
        Dataset<Row> sites = sameTypeOfPlacesAlgo.loadSites(sitesPath);

        if (algo.equals("same")) {
            Dataset<Row> visitedCategories = sameTypeOfPlacesAlgo.visitedCategories(visits, sites);
            Dataset<Row> placesToVisit = sameTypeOfPlacesAlgo.placesToVisit(visitedCategories, sites);
            sameTypeOfPlacesAlgo.filterPlacesToVisit(placesToVisit, visits).show(false);
        }
        else
        {
            Dataset<Row> rating = LikeMindedPersonsAlgo.getLikeMindedPersons(visits);
            LikeMindedPersonsAlgo.getLikeMindedRecommends(rating, visits, sites).show(false);
        }
    }
}
