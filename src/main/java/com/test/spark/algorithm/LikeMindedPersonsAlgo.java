package com.test.spark.algorithm;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.collect_list;
import static org.apache.spark.sql.functions.desc;

/**
 * Class implements "like-minded" persons algo.
 */
public class LikeMindedPersonsAlgo {
    /**
     * Creates dataset with rating of "like-minded" persons
     * @param visits list of visited places
     * @return dataset with rating
     */
    static public Dataset<Row> getLikeMindedPersons(Dataset<Row> visits) {
        Dataset<Row> visits_t = visits
                .withColumnRenamed("latitude", "latitude_t")
                .withColumnRenamed("personId", "mindedPersonId");
        return visits.join(
                visits_t, visits.col("latitude").equalTo(visits_t.col("latitude_t"))
                        .$amp$amp(visits.col("personId").notEqual(visits_t.col("mindedPersonId"))
                        )
        ).groupBy("personId", "mindedPersonId").count()
                .orderBy(asc("personId"), desc("count"))
                .withColumnRenamed("count", "rating")
                .alias("rating")
                ;
    }

    /**
     * Create recommendations based on "like-minded" persons rating
     * @param rating rating from 'getLikeMindedPersons'
     * @param visits list of visited persions
     * @param sites list of sites
     * @return recommendations
     */
    static public Dataset<Row> getLikeMindedRecommends(Dataset<Row> rating, Dataset<Row> visits, Dataset<Row> sites) {
        // look for places, that visits like-minded person
        Dataset<Row> l = visits.withColumnRenamed("personId", "personId_l")
                .withColumnRenamed("latitude", "mindedVisitLatitude");
        Dataset<Row> mindedVisitEnrich = rating.join(l,
                rating.col("mindedPersonId").equalTo(l.col("personId_l")),
                "left_outer"
        )
                .drop("personId_l");

        // check, if this place already visited by person
        l = l.withColumnRenamed("mindedVisitLatitude", "personVisitLatitude");
        Dataset<Row> r2 = mindedVisitEnrich.join(l,
                mindedVisitEnrich.col("mindedVisitLatitude").equalTo(l.col("personVisitLatitude"))
                        .$amp$amp(mindedVisitEnrich.col("personId").equalTo(l.col("personId_l")))
                ,
                "left_outer")
                .filter(r -> r.getAs("personId_l") == null)
                .drop("personId_l")
                .drop("personVisitLatitude")
                ;

        // get name of non-visited sites
        Dataset<Row> s = sites.withColumnRenamed("latitude", "latitude_r2");
        Dataset<Row> places = r2.join(s,
                r2.col("mindedVisitLatitude").equalTo(s.col("latitude_r2")))
                .drop("id")
                .drop("latitude_r2")
                .drop("category")
                .drop("mindedPersonId")
                .drop("rating")
                .drop("mindedVisitLatitude");

        return places.groupBy("personId")
                .agg(collect_list("name").as("recommendations"))
                .orderBy("personId");
    }
}
