package com.test.spark.algorithm;

import com.test.spark.domain.Visit;
import com.test.spark.domain.Site;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.collect_list;
import static org.apache.spark.sql.functions.desc;

public class SameTypeOfPlacesAlgo {
    private final SparkConf conf;
    private final JavaSparkContext sparkContext;
    private final SparkSession spark;
    public SameTypeOfPlacesAlgo() {
        conf = new SparkConf().setAppName("Counter").setMaster("local");
        sparkContext = new JavaSparkContext(conf);
        sparkContext.setLogLevel("ERROR");
        spark = SparkSession.builder().appName("Counter").getOrCreate();
    }

    public SameTypeOfPlacesAlgo(SparkConf conf, JavaSparkContext context, SparkSession spark) {
        this.conf = conf;
        sparkContext = context;
        this.spark = spark;
    }

    public Dataset<Row> loadVisits(String path) {
        return spark.createDataFrame(Visit.load(sparkContext, path), Visit.class);
    }

    public Dataset<Row> loadSites(String path) {
        return spark.createDataFrame(Site.load(sparkContext, path), Site.class);
    }

    /**
     * Counts rating of places categories for each person
     * @param visits list of places, visited by persons
     * @param sites total list of sites
     * @return dataset, where each row contains person, place category and "rating" of this category
     */
    public Dataset<Row> visitedCategories(Dataset<Row> visits, Dataset<Row> sites) {
        return visits.join(sites,
                sites.col("latitude").equalTo(visits.col("latitude")),
                "inner")
                .groupBy("personId", "category").count()
                .orderBy(desc("count"));
    }

    /**
     * Calculates places' recommendations for each person
     * @param categories categories from 'visitedCategories' function
     * @param sites total list of sites
     * @return dataset with recommendations. It consists of all places, including visited
     */
    public Dataset<Row> placesToVisit(Dataset<Row> categories, Dataset<Row> sites) {
        return categories.join(sites,
                sites.col("category").equalTo(categories.col("category"))
        )
                .orderBy(asc("personId"), desc("count"));
    }

    /**
     * Filter recommendations by dropping already visited places
     * @param recommends recommendations from 'placesToVisit'
     * @param visits list of visits
     * @return filtered dataset
     */
    public Dataset<Row> filterPlacesToVisit(Dataset<Row> recommends, Dataset<Row> visits) {
        Dataset<Row> visits_t = visits.withColumnRenamed("latitude", "vis_latitude")
                .withColumnRenamed("personId", "vis_personId");
        return recommends
                .join(visits_t,
                        recommends.col("latitude")
                                .equalTo(visits_t.col("vis_latitude"))
                        .$amp$amp(recommends.col("personId").equalTo(visits_t.col("vis_personId")))
                        , "left_outer"
                )
                .filter(r -> r.getAs("vis_latitude") == null)
                ;
    }
}
