package com.test.spark.domain;

import lombok.Builder;
import lombok.Data;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import scala.Serializable;

import java.util.Arrays;
import java.util.regex.Pattern;

@Data
@Builder
public class Site implements Serializable {
    private static final Pattern SEPARATOR = Pattern.compile("\\|");
    private int id;
    private String name;
    private String latitude;
    private String category;

    public static Site load(String s) {
        String[] strings = SEPARATOR.split(s);
        return Site.builder()
                .id(Integer.parseInt(strings[0]))
                .name(strings[1])
                .latitude(strings[2])
                .category(strings[3])
                .build();
    }

    public static JavaRDD<Site> load(JavaSparkContext sparkContext, String textFile) {
        JavaRDD<String> rdd = sparkContext.textFile(textFile);
        return rdd.filter(s -> !s.startsWith("#")).flatMap(s -> Arrays.asList(load(s)).iterator());
    }

    public Row toRow() {
        return RowFactory.create(id, name, latitude, category);
    }
}
