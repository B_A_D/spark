package com.test.spark.domain;

import lombok.Builder;
import lombok.Data;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Serializable;

import java.util.Arrays;
import java.util.regex.Pattern;

@Data
@Builder
public class Visit implements Serializable {

    private static final Pattern SEPARATOR = Pattern.compile("\\|");
    private int personId;
    private String latitude;

    static Visit load(String s) {
        String[] strings = SEPARATOR.split(s);
        return new Visit.VisitBuilder()
                .latitude(strings[1])
                .personId(Integer.parseInt(strings[0]))
                .build();
    }

    public static JavaRDD<Visit> load(JavaSparkContext sparkContext, String textFile) {
        JavaRDD<String> rdd = sparkContext.textFile(textFile);
        return rdd.filter(s -> !s.startsWith("#")).flatMap(s -> Arrays.asList(load(s)).iterator());
    }
}
