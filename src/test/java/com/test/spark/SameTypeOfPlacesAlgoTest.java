package com.test.spark;

import com.test.spark.algorithm.LikeMindedPersonsAlgo;
import com.test.spark.algorithm.SameTypeOfPlacesAlgo;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import scala.collection.mutable.WrappedArray;

import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.apache.spark.sql.types.Metadata.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

@RunWith(JUnit4ClassRunner.class)
public class SameTypeOfPlacesAlgoTest {
    private static SparkConf conf = new SparkConf()
            .setAppName("Counter")
            .setMaster("local");
    private static JavaSparkContext context = new JavaSparkContext(conf);
    private SparkSession spark;

    private Dataset<Row> sitesFrame;
    private Dataset<Row> visitsFrame;

    private SameTypeOfPlacesAlgo sameTypeOfPlacesAlgo = null;

    @Before
    public void init() {
        context.setLogLevel("ERROR");
        if (spark == null) {
            spark = SparkSession.builder()
                    .config(conf)
                    .getOrCreate();
        }
        loadSites();
        loadVisits();
        sameTypeOfPlacesAlgo  = new SameTypeOfPlacesAlgo(conf, context, spark);
    }
    @Test
    public void testVisitedCategories() {
        List<Row> visitedCategories = sameTypeOfPlacesAlgo.visitedCategories(visitsFrame, sitesFrame)
                .collectAsList();
        assertThat(visitedCategories.size(), is(4));
        assertThat(visitedCategories.stream()
                .filter(r -> r.getAs("category").equals("Museum")
                        && r.getAs("personId").equals(1)
                        && r.getAs("count").equals(2L))
                .count(), is(1L));
        assertThat(visitedCategories.stream()
                .filter(r -> r.getAs("category").equals("Kino")
                        && r.getAs("personId").equals(1)
                        && r.getAs("count").equals(1L))
                .count(), is(1L));
        assertThat(visitedCategories.stream()
                .filter(r -> r.getAs("category").equals("Kino")
                        && r.getAs("personId").equals(2)
                        && r.getAs("count").equals(1L))
                .count(), is(1L));
    }

    @Test
    public void testPlacesToVisit() {
        Dataset<Row> visitedCategories = sameTypeOfPlacesAlgo.visitedCategories(visitsFrame, sitesFrame);
        Dataset<Row> placesToVisit = sameTypeOfPlacesAlgo.placesToVisit(visitedCategories, sitesFrame);
        assertThat(placesToVisit.count(), is(12L));

        // try to check if result consists of some value.
        // this variant looks more compact than ".filter(r -> r.getAs("..."))"
        // but not so clear I think
        List<Row> rows = placesToVisit.collectAsList();
        Row row = RowFactory.create(1,"Museum",2L,3,"2 Museum",2.1,"Museum");
        assertThat(rows.contains(row), is(true));

        assertThat(placesToVisit.filter(r -> r.getAs("name").equals("1 Museum") &&
                r.getAs("personId").equals(1)).count(), is(1L));
        assertThat(placesToVisit.filter(r -> r.getAs("name").equals("2 Museum") &&
                r.getAs("personId").equals(1)).count(), is(1L));
    }

    @Test
    public void testFilterPlaces() {
        Dataset<Row> visitedCategories = sameTypeOfPlacesAlgo.visitedCategories(visitsFrame, sitesFrame);
        Dataset<Row> placesToVisit = sameTypeOfPlacesAlgo.placesToVisit(visitedCategories, sitesFrame);
        Dataset<Row> filterPlacesToVisit = sameTypeOfPlacesAlgo.filterPlacesToVisit(placesToVisit, visitsFrame);
        List<Row> recomends = filterPlacesToVisit.collectAsList();
        assertThat(recomends.size(), is(6));
        assertThat(recomends.stream()
                .filter(r -> r.getAs("personId").equals(1)
                        && r.getAs("name").equals("2 Museum")
                        && r.getAs("count").equals(2L)
                )
                .count(), is(1L));
        assertThat(recomends.stream()
                .filter(r -> r.getAs("personId").equals(1)
                        && r.getAs("name").equals("2 Kino")
                        && r.getAs("count").equals(1L)
                )
                .count(), is(1L));
        assertThat(recomends.stream()
                .filter(r -> r.getAs("personId").equals(2)
                        && r.getAs("name").equals("2 Kino")
                        && r.getAs("count").equals(1L)
                )
                .count(), is(1L));
    }

    @Test
    public void testLikeMindedPersons() {
        Dataset<Row> likeMindedPersons = LikeMindedPersonsAlgo.getLikeMindedPersons(visitsFrame);
        List<Row> rows = likeMindedPersons.collectAsList();
        assertThat(rows.stream()
        .filter(r -> r.getAs("personId").equals(1) &&
                r.getAs("mindedPersonId").equals(3) &&
                r.getAs("rating").equals(2L))
        .count(), is(1L));
        assertThat(rows.stream()
                .filter(r -> r.getAs("personId").equals(1) &&
                        r.getAs("mindedPersonId").equals(2) &&
                        r.getAs("rating").equals(1L))
                .count(), is(1L));
    }

    @Test
    public void testLikeMindedRecommends() {
        Dataset<Row> rating = LikeMindedPersonsAlgo.getLikeMindedPersons(visitsFrame);
        Dataset<Row> likeMindedRecommends = LikeMindedPersonsAlgo.getLikeMindedRecommends(rating, visitsFrame, sitesFrame);
        List<Row> rows = likeMindedRecommends.collectAsList();
        assertArrayEquals(new String[]{"3 Museum"}, (String[]) ((WrappedArray<String>) rows.get(0).getAs("recommendations")).array());
    }

    void loadVisits() {
        List<Row> visits = Arrays.asList(
                RowFactory.create(1, 1.1),
                RowFactory.create(1, 1.2),
                RowFactory.create(1, 1.3),
                RowFactory.create(1, 2.2),
                RowFactory.create(2, 2.2),
                RowFactory.create(3, 1.1),
                RowFactory.create(3, 1.3),
                RowFactory.create(3, 3.3)
        );
        StructField siteId = new StructField("personId", IntegerType, false, empty());
        StructField latitude = new StructField("latitude", DoubleType, false, empty());
        StructType site = new StructType(new StructField[]{siteId, latitude});

        visitsFrame = spark.createDataFrame(visits, site);
    }

    void loadSites() {
        List<Row> sites = Arrays.asList(
                RowFactory.create(1, "Pushkin Museum", 1.1, "Museum"),
                RowFactory.create(2, "1 Museum", 1.2, "Museum"),
                RowFactory.create(3, "2 Museum", 2.1, "Museum"),
                RowFactory.create(4, "1 Kino", 2.2, "Kino"),
                RowFactory.create(5, "2 Kino", 2.5, "Kino"),
                RowFactory.create(6, "3 Museum", 3.3, "Museum")
        );
        StructField siteId = new StructField("id", IntegerType, false, empty());
        StructField siteName = new StructField("name", StringType, false, empty());
        StructField latitude = new StructField("latitude", DoubleType, false, empty());
        StructField category = new StructField("category", StringType, false, empty());
        StructType site = new StructType(new StructField[]{siteId, siteName, latitude, category});

        sitesFrame = spark.createDataFrame(sites, site);
    }
}
